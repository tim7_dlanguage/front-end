import { useContext } from 'react'
import { AuthContext } from '../contexts/AuthContext'

export default function UseAuth() {
    const { apiUrl, token, setToken, userId, userName, userEmail, userRole } = useContext(AuthContext)

    return {
        apiUrl,
        token,
        setToken,
        userId,
        userName,
        userEmail,
        userRole
    }
}
