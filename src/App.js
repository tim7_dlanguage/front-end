import './App.css';
import { Route, Routes } from 'react-router-dom';

import Home from './pages/Home'
// import Header from './components/common/Header';
// import Footer from './components/common/Footer';
import Navbar from './components/common/Navbar';
import Detail from './pages/Detail';
import ForgetPass from './pages/ForgetPassword';
import ResetPass from './pages/resetPassword'
import Menu from './pages/Menu';
import Checkout from './pages/Checkout';
import EmailConfirm from './pages/EmailConfirm';
import { AuthProvider } from './contexts/AuthContext';
import SuccessPurcase from './pages/SuccessPurchase';
import { BreakpointProvider } from './contexts/BreakpointContext';
import Login from "./pages/Auth/Login/Index"
import Register from "./pages/Auth/Register/Index"
import Invoice from './pages/Invoice/Index';
import InvoiceDetail from './pages/InvoiceDetail/Index';
import Footer from './components/common/Footer';
import MyClass from './pages/MyClass';
import NavbarAdmin from './components/NavbarAdmin';
import CategoryTableAdmin from './admin/CategoryTableAdmin';
import ClassTableAdmin from './admin/CourseTableAdmin';
import Admin from './admin'
import User from './admin/User';
import InvoiceAdmin from './admin/Invoice';
import InvoiceDetailAdmin from './admin/Invoice/detail'
import PaymentMethod from './admin/PaymentMethod';

//tessssss

function App() {
  return (
    <div className="App">
      <BreakpointProvider>
        <AuthProvider>
          <Routes>
            <Route path='/' element={<Navbar />}>
              <Route index element={<Home />} />
              <Route path='menu/:id' element={<Menu />} />
              <Route path='detail/:id' element={<Detail />} />
              <Route path='checkout' element={<Checkout />} />
              <Route path='forgetpass' element={<ForgetPass />} />
              <Route path='resetpass' element={<ResetPass />} />
              <Route path='emailconfirm' element={<EmailConfirm />} />
              <Route path='successpurchase' element={<SuccessPurcase />} />
              <Route path='login' element={<Login />} />
              <Route path='register' element={<Register />} />
              <Route path='invoice' element={<Invoice />} />
              <Route path='invoicedetail/:id' element={<InvoiceDetail />} />
              <Route path='myclass' element={<MyClass />} />
              <Route path='admin' element={<Admin />} />
              <Route path='admin/user' element={<User />} />
              <Route path='admin/invoice' element={<InvoiceAdmin />} />
              <Route path='admin/invoice/detail/:id' element={<InvoiceDetailAdmin />} />
              <Route path='admin/paymentmethod' element={<PaymentMethod />} />
              <Route path='admin/category' element={<CategoryTableAdmin/>}/>
              <Route path='admin/course' element={<ClassTableAdmin />}/>
            </Route>
            {/* <Route path='/Admin' element={<NavbarAdmin/>}>
              <Route path='/Admin/category' element={<CategoryTableAdmin/>}/>
              <Route path='/Admin/course' element={<ClassTableAdmin />}/>
            </Route> */}
          </Routes>
          <Footer />
        </AuthProvider>
      </BreakpointProvider>
    </div>
  );
}

export default App;
