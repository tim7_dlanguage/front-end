import './Login.css';
import { Link, useNavigate } from "react-router-dom";
import Axios from "axios";
import useAuth from "../../../hooks/UseAuth";
import React, { useState } from 'react';
import { TextField, Button, Typography, Container, Grid } from '@mui/material';

export default function Login({ onLogin }){
    const [email,setEmail] = useState("");
    const [password,setPassword] = useState("");
    const navigate = useNavigate();

    const { setToken, apiUrl } = useAuth();

    const handleLogin = () => {
        let data = JSON.stringify({
            "email": email.toString(),
            "password": password.toString(),
          })
        
          let config = {
            method: 'post',
            maxBodyLength: Infinity,
            url: `${apiUrl}/User/Login`,
            headers: {
              'Content-Type': 'application/json'
            },
            data: data
          }

            Axios.request(config)
                .then((response) => {
                    setToken(response.data);
                    navigate('/');
                })
                .catch((error) => {
                    alert(error.response.data);
                })
    }
    
    return (
        // <div className="containerLogin">
        //     <div className="greet">Welcome   Back!</div>
        //     <div className="word">Please login first</div>
        //     <form className="form">
        //         <input type="email" className="email" placeholder="Email" name="email" value={email} onChange={(e) => setEmail(e.target.value)}/>
        //         <input type="password" className="password" placeholder="Password" name="password" value={password} onChange={(e) => setPassword(e.target.value)}/>
        //         <div className="word">Forgot Password? 
        //             <Link to="/resetEmail" className="linkForgot">
        //                 &nbsp;Click Here
        //             </Link>
        //         </div>
        //         <div className="containerSubmit">
        //             <button className="submitLogin" onClick={handelLogin}>Login</button>
        //         </div>
        //     </form>
        //     <div className="wordSignUp">Dont have account? 
        //         <Link to="/register" className="linkSignUp">
        //             &nbsp;Sign Up here
        //         </Link>
        //     </div>
        // </div>

        <Container maxWidth="sm" sx={{ marginTop: 10 }}>
            <Typography variant="h4" textAlign="center" marginBottom={2}sx={{ color: '#226957' }}>
                Welcome Back!
            </Typography>
            <Typography variant="body1" textAlign="center" marginBottom={2}>
                Please login first
            </Typography>
                <Grid container spacing={2}>
                <Grid item xs={12}>
                    <TextField
                    fullWidth
                    variant="outlined"
                    label="Email"
                    type="email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    />
                </Grid>
                <Grid item xs={12}>
                    <TextField
                    fullWidth
                    variant="outlined"
                    label="Password"
                    type="password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    />
                </Grid>
                <Grid item xs={12}>
                    <Typography variant="body1">
                    Forgot Password? <Link to="/forgetpass">Click Here</Link>
                    </Typography>
                </Grid>
                <Grid item xs={12} sx={{ textAlign: 'right' }}>
                    <Button onClick={handleLogin} variant="contained" color="primary" type="submit">
                    Login
                    </Button>
                </Grid>
                </Grid>
            <Typography variant="body1" textAlign="center" marginTop={2}>
                Don't have an account? <Link to="/register">Sign Up here</Link>
            </Typography>
        </Container>
    );
}
