import React, { useState } from "react";
import './Register.css';
import { Link, useNavigate } from "react-router-dom";
import Axios from "axios";
import UseAuth from "../../../hooks/UseAuth";
import { Container, Typography, TextField, Button, Grid } from '@mui/material';


export default function Register(){
    const [customerName,setName] = useState("");
    const [email,setEmail] = useState("");
    const [password,setPassword] = useState("");
    const [copassword,setcoPassword] = useState("");
    const navigate = useNavigate();

    const { apiUrl } = UseAuth()
    
    const confirmPassword = (e) => {
        
        if(e.target.value !== password){
            setcoPassword("Password Tidak Sama");
        } else{
            setcoPassword("");
        }
    }

    const handleRegister = () => {
        let data = JSON.stringify({
            "name": customerName,
            "email": email,
            "password": password,
          })
        
          let config = {
            method: 'post',
            maxBodyLength: Infinity,
            url: `${apiUrl}/User/Register`,
            headers: {
              'Content-Type': 'application/json'
            },
            data: data
          }

            Axios.request(config)
                .then((response) => {
                    window.alert("Register Berhasil, silahkan verifikasi email anda")
                    navigate('/')
                })
                .catch((error) => {
                    alert(error.response.data);
                })
    }

    return (
        <Container maxWidth="sm" sx={{ marginTop: 10 }}>
            <Typography variant="h4" textAlign="center" marginBottom={2} sx={{ color: '#226957' }}>
                Let's Join <b>D'Language</b>
            </Typography>
            <Typography variant="body1" textAlign="center" marginBottom={2}>
                Please register first
            </Typography>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                <TextField
                    fullWidth
                    variant="outlined"
                    label="Name"
                    type="text"
                    value={customerName}
                    onChange={(e) => setName(e.target.value)}
                />
                </Grid>
                <Grid item xs={12}>
                <TextField
                    fullWidth
                    variant="outlined"
                    label="Email"
                    type="email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                />
                </Grid>
                <Grid item xs={12}>
                <TextField
                    fullWidth
                    variant="outlined"
                    label="Password"
                    type="password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                />
                </Grid>
                <Grid item xs={12}>
                <TextField
                    fullWidth
                    variant="outlined"
                    label="Confirm Password"
                    type="password"
                    onChange={(e) => confirmPassword(e)}
                />
                {copassword && <Typography color="error">{copassword}</Typography>}
                </Grid>
                <Grid item xs={12} sx={{ textAlign: 'right' }}>
                <Button variant="contained" color="primary" onClick={handleRegister}>
                    Sign Up
                </Button>
                </Grid>
            </Grid>
            <Typography variant="body1" textAlign="center" marginTop={2}>
                Have an account? <Link to="/login">Login here</Link>
            </Typography>
        </Container>
    );
}
