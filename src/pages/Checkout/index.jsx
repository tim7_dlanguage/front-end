import React, { useEffect, useState } from 'react';
import { Box, Grid, Checkbox, Typography, IconButton, Button, useMediaQuery } from '@mui/material';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import Axios from 'axios';
import PaymentModal from '../../components/common/PaymentModal';
import UseAuth from '../../hooks/UseAuth';

const Checkout = () => {
  const [products, setProducts] = useState([])
  const [selectAll, setSelectAll] = useState(false)
  const [selectedProducts, setSelectedProducts] = useState([])
  const [selectedProductsId, setSelectedProductsId] = useState([])
  const [openDialog, setOpenDialog] = useState(false)
  const isMobile = useMediaQuery('(max-width: 768px)')
  const { apiUrl, token, userId } = UseAuth()

  useEffect(() => {
    let config = {
      method: 'get',
      maxBodyLength: Infinity,
      url: `${apiUrl}/Cart/GetCart?user_id=${userId}\n`,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }

    Axios.request(config)
      .then((response) => {
        setProducts(response.data)
      })
      .catch((error) => {
        console.log(error)
      })
  })

  const handleSelectAllChange = (event) => {
    const checked = event.target.checked;
    setSelectAll(checked);

    // Add your logic for handling the select all functionality here
    const updatedSelectedProducts = checked
      ? Array.from(Array(products.length).keys())
      : []
    const updatedSelectedProductsId = checked
      ? Array.from(products.map((item) => (item.id).toString()))
      : []

    setSelectedProducts(updatedSelectedProducts)
    setSelectedProductsId(updatedSelectedProductsId)
  };

  const handleProductSelect = (event, index) => {
    const checked = event.target.checked;
    const updatedSelectedProducts = checked
      ? [...selectedProducts, index]
      : selectedProducts.filter((selectedIndex) => selectedIndex !== index);

    const updatedSelectedProductsId = checked
      ? [...selectedProductsId, (products[index].id).toString()]
      : selectedProductsId.filter((id) => id !== products[index].id)

    setSelectedProducts(updatedSelectedProducts);
    setSelectedProductsId(updatedSelectedProductsId)
  };

  // const handleDelete = () => {
  //   // Remove the selected products from the 'products' array
  //   // const updatedProducts = products.filter((product, index) => !selectedProducts.includes(index));
  //   // Clear the selected products array
  //   console.log(deleted)
  //   setSelectedProducts([]);
  //   // Update the 'products' array after removing the selected products
  //   // You can handle deletion logic here, e.g., send a request to a server to delete the products.
  // };

  function handleDelete(cartId) {
    console.log(cartId)
    let config = {
      method: 'delete',
      maxBodyLength: Infinity,
      url: `${apiUrl}/Cart/DeleteCartById?cart_id=${cartId}\n`,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }

    Axios.request(config)
      .then((response) => {
        console.log(response.data)
      })
      .catch((error) => {
        alert(error)
      })

  }

  // Calculate the total price based on selected products
  const getTotalPrice = () => {
    const totalPrice = selectedProducts.reduce(
      (acc, currentIndex) => acc + parseInt(products[currentIndex].course.price),
      0
    );
    return totalPrice;
  };

  const handleOpenDialog = () => {
    setOpenDialog(true);
  };

  const handleCloseDialog = () => {
    setOpenDialog(false);
  };

  return (
    <Box sx={{ marginTop: "100px", marginLeft: '72px', marginRight: '72px' }}>
      <Grid container alignItems="center">
        <Grid item>
          <Checkbox
            checked={selectAll}
            onChange={handleSelectAllChange} />
        </Grid>
        <Grid item>
          <Typography sx={{ fontFamily: 'Poppins', fontSize: '20px', fontWeight: 400, marginLeft: '12px' }}>
            Pilih Semua
          </Typography>
        </Grid>
      </Grid>
      <Box>
        {/* Map through products */}
        {products.map((product, index) => (
          <Box
            key={index}
            sx={{ display: 'flex', padding: '16px', borderBottom: '1px solid #BDBDBD', marginTop: '24px', marginLeft: '16px', flex: 1 }}
          >
            <Checkbox
              checked={selectedProducts.includes(index)}
              onChange={(event) => handleProductSelect(event, index)}
            />
            <img
              src={product.course.image}
              alt="DLanguage"
              style={{ width: '300px', borderRadius: '8px', marginLeft: '16px' }}
            />
            <Box
              sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-between', alignItems: 'flex-start', marginLeft: '25px', flex: 1 }}
            >
              <Typography
                sx={{ fontFamily: 'Montserrat', fontSize: '16px', fontWeight: 400, color: 'Gray 3' }}>
                {product.course.type}
              </Typography>
              <Typography
                sx={{ fontFamily: 'Montserrat', fontSize: '24px', fontWeight: 600 }}>
                {product.course.name}
              </Typography>
              <Typography
                sx={{ fontFamily: 'Montserrat', fontSize: '16px', fontWeight: 400, color: 'Gray 2' }}>
                {product.course_date}
              </Typography>
              <Typography
                sx={{ fontFamily: 'Montserrat', fontSize: '20px', fontWeight: 600, color: '#FABC1D' }}>
                IDR {product.course.price}
              </Typography>
            </Box>
            <IconButton color="error" aria-label="delete" onClick={() => handleDelete(product.id)}>
              <DeleteForeverIcon />
            </IconButton>
          </Box>
        ))}

        {/* Display total price and Pay Now Button */}
        <Box sx={{ marginTop: '126px', marginLeft: '72px', marginRight: '72px', display: 'flex', flexDirection: isMobile ? 'column' : 'row' }}>
          <Grid container alignItems="center">
            <Grid item xs={isMobile ? 12 : 10} sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
              <Typography sx={{ fontSize: '18px', fontWeight: 400, textAlign: isMobile ? 'center' : 'left', marginBottom: isMobile ? '10px' : '0' }}>
                {'Total Price'}
              </Typography>
              <Typography sx={{ fontSize: '24px', fontWeight: 600, color: '#226957', textAlign: isMobile ? 'center' : 'left', marginBottom: isMobile ? '10px' : '0' }}>
                IDR {getTotalPrice()}
              </Typography>
            </Grid>
            <Grid item xs={isMobile ? 12 : 2}>
              <Button variant="contained" color="primary"
                sx={{
                  backgroundColor: "#FABC1D",
                  fontFamily: "Montserrat",
                  minWidth: isMobile ? "100%" : "130px",
                }} onClick={handleOpenDialog}>
                Pay Now
              </Button>
            </Grid>
          </Grid>
        </Box>
      </Box>

      {/* Payment Modal */}
      <PaymentModal openDialog={openDialog} handleCloseDialog={handleCloseDialog} selectedProductsId={selectedProductsId} />
    </Box>
  );
};
export default Checkout
