import React, { useEffect, useState } from 'react'
import { Box, Button, Container, Grid, TextField, Typography } from '@mui/material'
import { useNavigate } from 'react-router-dom'
import Axios from 'axios'
import UseAuth from '../../hooks/UseAuth'

const ForgetPassword = () => {
    const { apiUrl } = UseAuth()
    const [input, setInput] = useState('')
    const [isValid, setIsValid] = useState()
    const navigate = useNavigate()

    useEffect(() => {
        setIsValid(input
            .toLowerCase()
            .match(
                /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            ) ? true : false
        )
    }, [input])

    const handleClick = () => {
        if (isValid) {
            Axios.post(`${apiUrl}/User/ForgetPassword?email=${input}`).then((res) => {
                alert(res.data)
            })
        } else {
            alert('Your email is not valid')
        }
    }

    const handleCancelClick = () => {
        navigate('/')
    }

    const handleChange = (e) => {
        console.log(input)
        console.log(isValid)
        setInput(e.target.value)
    }

    return (
        <Container maxWidth="sm">
            <Box sx={{
                marginTop: 20,
                display: "flex",
                alignItems: "center",
                flexDirection: "column"
            }}>
                <Grid container justifyContent="flex-start" alignItems="center" sx={{ mb: 5 }}>
                    <Grid item xs={12}>
                        <Typography sx={{
                            fontFamily: 'Montserrat',
                            fontSize: '24px',
                            fontWeight: '400',
                            lineHeight: '29.26px',
                            letterSpacing: '0em',
                            textAlign: 'left',
                            color: '#333333',
                            width: '190px',
                            height: '29px'
                        }}>
                            Reset Password
                        </Typography>
                        <Typography sx={{
                            fontFamily: 'Montserrat',
                            fontSize: '16px',
                            fontWeight: '400',
                            lineHeight: '19.5px',
                            letterSpacing: '0em',
                            textAlign: 'left',
                            color: '##4F4F4F',
                            width: '299px',
                            height: '20px'
                        }}>
                            Please enter your email address
                        </Typography>
                    </Grid>
                </Grid>
                <TextField
                    id="email"
                    label="Email"
                    variant="outlined"
                    fullWidth
                    required
                    margin="normal"
                    autoFocus
                    name="email"
                    type="text"
                    error={input.length > 0 && !isValid}
                    helperText={input.length > 0 && !isValid ? 'Email not Valid!' : ' '}
                    onChange={handleChange}
                />
                <div style={{ display: 'flex', alignSelf: 'flex-end', marginTop: '10px' }}>
                    <Button
                        type="submit"
                        variant="contained"
                        size="medium"
                        sx={{
                            mt: 3, mb: 1, marginRight: '10px', backgroundColor: '#EA9E1F',
                            color: 'white', textTransform: 'none', borderRadius: '5px', width: "140px", height: "38px"
                        }}
                        onClick={handleCancelClick}
                    >
                        Cancel
                    </Button>
                    <Button
                        type="submit"
                        variant="contained"
                        size="medium"
                        onClick={handleClick}
                        sx={{
                            mt: 3, mb: 1, backgroundColor: '#226957',
                            color: 'white', textTransform: 'none', borderRadius: '5px', width: "140px", height: "38px"
                        }}
                    >
                        Confirm
                    </Button>
                </div>
            </Box>
        </Container>
    )
}

export default ForgetPassword
