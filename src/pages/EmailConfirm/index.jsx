import React from 'react'
import succesTransaction from '../../assets/EmailConfirm.png'
import { Typography, Button } from '@mui/material'
import { useNavigate } from 'react-router-dom'

const EmailConfirm = () => {
    const navigate = useNavigate()

    const handleLogin = () => {
        navigate('/login')
    };

    return (
        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
            <img src={succesTransaction} alt="." style={{ marginTop: '54px' }} />
            <Typography style={{ marginTop: '44px', color: '#226957', fontSize: '24px' }}>
                Email Confirmation Success
            </Typography>
            <Typography style={{ marginTop: '8px', fontSize: '16px', color: '#4F4F4F' }}>
                Thanks for confirmation your email. Please login first
            </Typography>
            <Button style={{ marginTop: '40px', background: '#226957', width: '140px', height: '38px', padding: '16px 24px', borderRadius: '6px', color: '#ffffff' }} onClick={handleLogin}>Login Here</Button>
        </div>
    )
}

export default EmailConfirm
