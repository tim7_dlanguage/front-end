import React, { useContext } from 'react'
import { useLocation } from 'react-router-dom'
import ListProduct from '../../components/common/ListProduct'
import { AuthContext, AuthProvider } from '../../contexts/AuthContext'

const Home = () => {
    const { pathname } = useLocation()
    const { userName } = useContext(AuthContext)
    console.log(pathname)

    return (
        <div>
            <AuthProvider>
                <h1>{userName==='' ? "Login First!": "Hi "+userName}</h1>
                <ListProduct productType='' />
            </AuthProvider>
        </div>
    )
}

export default Home