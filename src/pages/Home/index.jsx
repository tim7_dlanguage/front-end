import React, { useEffect, useState } from 'react';
import './Home.css';
import ListProduct from '../../components/common/ListProduct';
import ListType from '../../components/common/ListType';
import { Typography } from '@mui/material';
import UseAuth from '../../hooks/UseAuth';


const Index = () => {
    const { token, userName } = UseAuth()

    //const [typeProductData, setTypeProductData] = useState([]);

    // useEffect(() => {

    //     // Fetch data from the second API endpoint
    //     axios
    //         .get('http://52.237.194.35:2022/api/product/GetTypeProduct')
    //         .then((response) => {
    //             // Update the typeProductData state with the fetched data
    //             setTypeProductData(response.data);
    //         })
    //         .catch((error) => {
    //             console.error('Error fetching type product data:', error);
    //         });
    // }, []); // Empty dependency array ensures the effect runs once when the component mounts


    function CountItem({ targetValue, description }) {
        const [countValue, setCountValue] = useState(0);

        useEffect(() => {
            let currentValue = 0;
            const animationInterval = setInterval(() => {
                if (currentValue <= targetValue) {
                    setCountValue(currentValue);
                    currentValue++;
                } else {
                    clearInterval(animationInterval);
                }
            }, 20);

            return () => {
                clearInterval(animationInterval);
            };
        }, [targetValue]);

        return (
            <div className="homepage-count-item">
                <span className="count">{countValue}+</span>
                <p>{description}</p>
            </div>
        );
    }


    return (
        <div className='Homepage'>
            <Typography variant='h4' textAlign="center" marginBottom={2} sx={{ color: '#226957' }}>
                {token === '' ? "Login First!" : "Hi " + userName}
            </Typography>
            <div className="homepage-banner">
                <div className="banner-detail">
                    <h1>
                        Learn different languages ​​to hone <br />
                        your communication skills
                    </h1>
                    <h6>
                        All the languages ​​you are looking for are available here, so what are you waiting for <br />
                        and immediately improve your language skills
                    </h6>
                </div>
            </div>
            <div className="homepage-count">
                <CountItem targetValue={100} description="Choose the class you like and get the skills" />
                <CountItem targetValue={50} description="Having teachers who are highly skilled and competent in the language" />
                <CountItem targetValue={10} description="Many alumni become ministry employees because of their excellent language skills" />
            </div>
            <div className="recommended">
                <div className="recommended-title">
                    <h5>Recommended Class</h5>
                </div>
                <ListProduct productType='' />
            </div>
            <div className="homepage-benefit">
                <div className="benefit-wrapper">
                    <div className="benefit-row">
                        <h3>
                            Gets your best benefit
                        </h3>
                        <p className="benefit-p">
                            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
                        </p>
                    </div>
                </div>
            </div>
            <div className="available-language">
                <Typography sx={{ fontSize: '24px', fontWeight: 600, color: '#226957' }}>
                    All Available Language
                </Typography>
                <ListType />
            </div>
        </div>
    )
}

export default Index