import { Box, Button, Container, Grid, TextField, Typography } from '@mui/material'
import Axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useLocation, useNavigate } from 'react-router-dom'
import UseAuth from '../../hooks/UseAuth'

const ResetPassword = () => {
    const { apiUrl } = UseAuth()
    const [newPass, setNewPass] = useState('')
    const [confirmPass, setConfirmPass] = useState('')
    const [isValid, setIsValid] = useState()
    const navigate = useNavigate()
    const location = useLocation()
    const emailParams = new URLSearchParams(location.search).get('email')

    useEffect(() => {
        setIsValid(newPass.length >= 8 && newPass === confirmPass ? true : false)
    }, [newPass, confirmPass])

    const handleClick = () => {
        if (isValid) {
            let data = JSON.stringify({
                "email": emailParams,
                "password": newPass,
                "confirmPassword": confirmPass
            })

            let config = {
                method: 'post',
                maxBodyLength: Infinity,
                url: `${apiUrl}/User/ResetPassword`,
                headers: {
                    'Content-Type': 'application/json'
                },
                data: data
            }

            Axios.request(config)
                .then((response) => {
                    //alert(response.data)
                    console.log(JSON.stringify(response.data))
                    navigate('/emailconfirm')
                })
                .catch((error) => {
                    alert(error)
                    console.log(error);
                })
        } else {
            if (newPass === '') {
                alert("Please fill column password fist")
            } else {
                alert("Your password is Doesn't Match!")
            }
        }
    }

    const handleCancelClick = () => {
        navigate('/')
    }

    const handleNPchange = (e) => {
        setNewPass(e.target.value)
    }
    const handleCPchange = (e) => {
        setConfirmPass(e.target.value)
    }

    return (
        <Container maxWidth="sm">
            <Box sx={{
                marginTop: 20,
                display: "flex",
                alignItems: "center",
                flexDirection: "column"
            }}>
                <Grid container justifyContent="flex-start" alignItems="center" sx={{ mb: 2 }}>
                    <Grid item xs={12}>
                        <Typography style={{
                            fontFamily: 'Montserrat', fontSize: '24px',
                            fontWeight: 400,
                            lineHeight: '29px',
                            letterSpacing: '0em',
                            textAlign: 'left',
                        }}>
                            Create Password
                        </Typography>
                    </Grid>
                </Grid>
                <TextField
                    id="newpassword"
                    label="New Password"
                    variant="outlined"
                    fullWidth
                    required
                    margin="normal"
                    autoFocus
                    name="password"
                    type="password"
                    error={newPass.length > 0 && newPass.length < 8}
                    helperText={newPass.length > 0 && newPass.length < 8 ? 'Password must contain at least 8 characters!' : ''}
                    value={newPass}
                    onChange={handleNPchange}
                />
                <TextField
                    id="Confirmnewpassword"
                    label="Confirm New Password"
                    variant="outlined"
                    fullWidth
                    required
                    margin="normal"
                    autoFocus
                    name="password"
                    type="password"
                    error={newPass.length >= 8 && newPass !== confirmPass && confirmPass.length > 0}
                    helperText={newPass.length >= 8 && newPass !== confirmPass && confirmPass.length > 0 ? "Password doesn't match!" : ' '}
                    value={confirmPass}
                    onChange={handleCPchange}
                />

                <div style={{ display: 'flex', alignSelf: 'flex-end', marginTop: '10px' }}>
                    <Button
                        type="submit"
                        variant="contained"
                        size="medium"
                        sx={{
                            mt: 3, mb: 1, marginRight: '10px', backgroundColor: '#EA9E1F',
                            color: 'white', textTransform: 'none', borderRadius: '5px', width: "140px", height: "38px"
                        }}
                        onClick={handleCancelClick}
                    >
                        Cancel
                    </Button>
                    <Box sx={{ width: '24px' }} />
                    <Button
                        type="submit"
                        variant="contained"
                        size="medium"
                        sx={{
                            mt: 3, mb: 1,
                            backgroundColor: '#226957',
                            color: 'white',
                            textTransform: 'none',
                            borderRadius: '5px',
                            width: "140px",
                            height: "38px",
                            marginBottom: '175px'
                        }}
                        onClick={handleClick}
                    >
                        Submit
                    </Button>
                </div>
            </Box>
        </Container>
    )
}

export default ResetPassword
