import React, { useContext, useEffect, useState } from 'react'
import ListProduct from '../../components/common/ListProduct'
import { useParams } from 'react-router-dom'
import Axios from 'axios';
import { AuthContext } from '../../contexts/AuthContext';
import BannerListMenuClass from "../../assets/BannerListMenuClass.jpeg"
import { Box, Typography } from '@mui/material'


const Menu = () => {
    const { id } = useParams()
    const { apiUrl } = useContext(AuthContext)
    const [data, setData] = useState([])

    useEffect(() => {
        Axios.get(`${apiUrl}/CourseType/getcoursetypebyid?id=${id}`).then(res => {
            setData(res.data)
        })
    })

    return (
        <div style={{}}> {/* Adding margin to the outer container */}
            <Box sx={{ width: '100%' }}> {/* Adding padding to the Box component */}
                <img src={BannerListMenuClass} alt='dlanguage' style={{ width: '100%', height: 'auto' }} />
                <Typography variant="h5" sx={{ fontWeight: 600, fontSize: 24, marginTop: 2, px: '3vw', textAlign: 'left' }}>
                    {data.type_name}
                </Typography>
                <p style={{ margin: '', paddingLeft: '3vw', paddingRight: '3vw', textAlign: 'left' }}>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </p>
            </Box>
            <br />
            <Typography variant="h4" sx={{ fontSize: 32, fontWeight: 600, marginTop: 3 }}>
                Classes You Might Like
            </Typography>
            <br />
            {/* Assuming ListProduct is a Material-UI component */}
            <ListProduct productType={data.type_name} />
        </div>
    )
}

export default Menu
