import { useNavigate } from 'react-router-dom';
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { Box, Button, Typography } from '@mui/material';
import Axios from 'axios';
import React, { useContext, useEffect, useState } from 'react'
import { AuthContext } from '../../contexts/AuthContext';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: "#226957",
    color: theme.palette.common.white,
    fontWeight: 700,
    fontSize: 16,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 16,
    height: 55,
    width: 100
  },
}));

const StyledTableRow = styled(TableRow)(() => ({
  '&:nth-of-type(even)': {
    backgroundColor: '#EA9E1F33',
  },
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));

function Invoice() {
  const { apiUrl, userId } = useContext(AuthContext);
  const navigate = useNavigate(); // Initialize useNavigate inside the functional component
  const [listInvoice, setListInvoice] = useState([])

  function openDetail(invoice_id) {
    navigate(`/invoicedetail/${invoice_id}`)
  }

  useEffect(() => {
    Axios.get(`${apiUrl}/Invoice/GetInvoice?user_id=${userId}`).then(res => {
      setListInvoice(res.data)
    })
  })

  return (
    <div>
      <br />
      <TableContainer component={Paper} sx={{ boxShadow: "none", maxWidth: 1400, mx: "auto" }}>
        <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-start' }}>
          <Typography sx={{ fontSize: '16px', fontWeight: '600' }}>
            {"Home > Invoice"}
          </Typography>
          <br />
          <Typography sx={{ fontSize: '20px', fontWeight: '600' }}>
            Menu Invoice
          </Typography>
          <br />
        </Box>
        <Table aria-label="customized table">
          <TableHead>
            <TableRow>
              <StyledTableCell align='center'>No</StyledTableCell>
              <StyledTableCell align='center'>No. Invoice</StyledTableCell>
              <StyledTableCell align='center'>Date</StyledTableCell>
              <StyledTableCell align='center'>Total Course</StyledTableCell>
              <StyledTableCell align='center'>Total Price</StyledTableCell>
              <StyledTableCell align='center'>Action</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {listInvoice.map((row, index) => (
              <StyledTableRow key={index}>
                <StyledTableCell component="th" scope="row" align='center'>
                  {index + 1}
                </StyledTableCell>
                <StyledTableCell align='center'>{row.invoice_id}</StyledTableCell>
                <StyledTableCell align='center'>{row.invoice_date}</StyledTableCell>
                <StyledTableCell align='center'>{row.total_course}</StyledTableCell>
                <StyledTableCell align='center'>{"IDR " + row.total_price.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')}</StyledTableCell>
                <StyledTableCell align='center'>
                  <Button
                    sx={{
                      backgroundColor: "#EA9E1F",
                      color: "white",
                      width: 150,
                      height: 40,
                      borderRadius: 2,
                      '&:hover': { backgroundColor: "#EA9E1F" },
                    }}
                    onClick={() => openDetail(row.invoice_id)}
                  >
                    Details
                  </Button>
                </StyledTableCell>
              </StyledTableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
}

export default Invoice;
