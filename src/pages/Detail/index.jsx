import { Autocomplete, Box, Button, Grid, TextField, ThemeProvider, Typography } from '@mui/material'
import Axios from 'axios'
import React, { useContext, useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import UseAuth from '../../hooks/UseAuth'
import ListProduct from '../../components/common/ListProduct'
import { BreakpointhContext } from '../../contexts/BreakpointContext'

const iconMargin = {
    flexGrow: 1,
    mr: 2,
    fontSize: '16px',
    color: '#226957',
    fontWeight: '500',
    textTransform: 'none',
    maxWidth: '150px'
}

const Detail = () => {
    const { id } = useParams()
    const navigate = useNavigate()
    const [detail, setDetail] = useState({})
    const [schedule, setSchedule] = useState([])
    const [selectedSchedule, setSelectedSchedule] = useState('')
    const { apiUrl, token, userId, userName } = UseAuth()
    const { isMobile } = useContext(BreakpointhContext)

    useEffect(() => {
        Axios.get(`${apiUrl}/Course/getcoursebyid?id=${id}`).then(res => {
            setDetail(res.data)
        })
        Axios.get(`${apiUrl}/Cart/GetSchedule`).then(res => {
            setSchedule(res.data)
        })
    })

    const handleAddToCart = () => {
        if (token === '') {
            alert("Please Login First!")
            navigate('/login')
        } else if (selectedSchedule === '') {
            alert("Select Schedule First")
        } else {
            let config = {
                method: 'post',
                maxBodyLength: Infinity,
                url: `${apiUrl}/Cart/AddCourseToCart?user_id=${userId}&course_id=${detail.id}&schedule_id=${selectedSchedule}\n`,
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            }

            Axios.request(config)
                .then((response) => {
                    console.log(JSON.stringify(response.data));
                })
                .catch((error) => {
                    console.log(error);
                })
        }
    }

    const handleBuyNow = () => {
        if (token === '') {
            alert("Please Login First!")
            navigate('/login')
        } else if (selectedSchedule === '') {
            alert("Select Schedule First")
        } else {
            let config = {
                method: 'post',
                maxBodyLength: Infinity,
                url: `${apiUrl}/Cart/AddCourseToCart?user_id=${userId}&course_id=${detail.id}&schedule_id=${selectedSchedule}\n`,
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            }

            Axios.request(config)
                .then((response) => {
                    console.log(JSON.stringify(response.data))
                })
                .catch((error) => {
                    console.log(JSON.stringify(error))
                })

            navigate('/checkout')
        }
    }

    return (
        <Box sx={{ mx: '70px' }}>
            <Typography sx={{
                color: '#EA9E1F',
                fontSize: '24px',
                fontWeight: '600'
            }}>{token !== '' ? "For " + userName : "Login First!"}</Typography>
            <Grid container sx={{ display: 'flex', flexDirection: 'row' }}>
                <img src={detail.image} alt='dlanguage' style={{ maxWidth: isMobile ? '300px' : '400px', marginRight: '20px' }} />
                <Grid item sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-between', alignItems: 'flex-start' }}>
                    <Typography sx={{
                        color: '#828282',
                        fontSize: '16px',
                        fontWeight: '400'
                    }}>
                        {detail.type}
                    </Typography>
                    <Typography sx={{
                        color: '#5B4947',
                        fontSize: '24px',
                        fontWeight: '600'
                    }}>
                        {detail.name}
                    </Typography>
                    <Typography sx={{
                        color: '#EA9E1F',
                        fontSize: '24px',
                        fontWeight: '600'
                    }}>
                        IDR {detail.price}
                    </Typography>
                    <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                        <Autocomplete
                            disablePortal
                            id="combo-box-demo"
                            options={schedule}
                            getOptionLabel={(option) => option.label}
                            isOptionEqualToValue={(option, value) => option.id === value.id}
                            sx={{ width: ThemeProvider.length }}
                            renderInput={(params) =>
                                <TextField {...params} label="Select Schedule" />}
                            onChange={(event, value) => setSelectedSchedule(value === null ? '' : value.id)}
                        />
                        <br />
                        <Box sx={{ display: 'flex', flexDirection: 'row' }}>
                            <Button
                                variant="contained"
                                component="div"
                                sx={iconMargin}
                                style={{
                                    borderRadius: '8px',
                                    backgroundColor: '#226957',
                                    color: '#FFFFFF',
                                    width: '175px',
                                    height: '40px'
                                }}
                                onClick={handleAddToCart}
                            >
                                Add to Cart
                            </Button>
                            <Button
                                variant="contained"
                                component="div"
                                sx={iconMargin}
                                style={{
                                    borderRadius: '8px',
                                    backgroundColor: '#EA9E1F',
                                    color: '#FFFFFF',
                                    width: '175px',
                                    height: '40px'
                                }}
                                onClick={handleBuyNow}
                            >
                                Buy Now
                            </Button>
                        </Box>
                    </Box>
                </Grid>

            </Grid>
            <br />
            <br />
            <Typography sx={{
                fontSize: '24px',
                fontWeight: '600',
                textAlign: 'left'
            }}>
                Description
            </Typography>
            <br />
            <Typography sx={{
                fontSize: '16px',
                fontWeight: '400',
                textAlign: 'left'
            }}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </Typography>
            <br />
            <Typography sx={{
                fontSize: '16px',
                fontWeight: '400',
                textAlign: 'left'
            }}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </Typography>

            <br />
            <br />
            <br />
            <br />
            <br />

            <hr
                style={{
                    color: '#226957',
                    backgroundColor: '#226957',
                    height: 1
                }}
            />

            <br />
            <br />

            <Typography sx={{
                color: '#226957',
                fontSize: '24px',
                fontWeight: '600'
            }}>
                Another Class for You
            </Typography>
            <ListProduct productType='' />
        </Box>
    )
}

// const top100Films = [
//     { label: 'The Shawshank Redemption', year: 1994 },
//     { label: 'The Godfather', year: 1972 },
//     { label: 'The Godfather: Part II', year: 1974 },
//     { label: 'The Dark Knight', year: 2008 },
//     { label: '12 Angry Men', year: 1957 },
//     { label: "Schindler's List", year: 1993 },
//     { label: 'Pulp Fiction', year: 1994 },
//     {
//         label: 'The Lord of the Rings: The Return of the King',
//         year: 2003,
//     },
//     { label: 'The Good, the Bad and the Ugly', year: 1966 },
//     { label: 'Fight Club', year: 1999 },
//     {
//         label: 'The Lord of the Rings: The Fellowship of the Ring',
//         year: 2001,
//     },
//     {
//         label: 'Star Wars: Episode V - The Empire Strikes Back',
//         year: 1980,
//     },
//     { label: 'Forrest Gump', year: 1994 },
//     { label: 'Inception', year: 2010 },
//     {
//         label: 'The Lord of the Rings: The Two Towers',
//         year: 2002,
//     },
//     { label: "One Flew Over the Cuckoo's Nest", year: 1975 },
//     { label: 'Goodfellas', year: 1990 },
//     { label: 'The Matrix', year: 1999 },
//     { label: 'Seven Samurai', year: 1954 },
//     {
//         label: 'Star Wars: Episode IV - A New Hope',
//         year: 1977,
//     },
//     { label: 'City of God', year: 2002 },
//     { label: 'Se7en', year: 1995 },
//     { label: 'The Silence of the Lambs', year: 1991 },
//     { label: "It's a Wonderful Life", year: 1946 },
//     { label: 'Life Is Beautiful', year: 1997 },
//     { label: 'The Usual Suspects', year: 1995 },
//     { label: 'Léon: The Professional', year: 1994 },
//     { label: 'Spirited Away', year: 2001 },
//     { label: 'Saving Private Ryan', year: 1998 },
//     { label: 'Once Upon a Time in the West', year: 1968 },
//     { label: 'American History X', year: 1998 },
//     { label: 'Interstellar', year: 2014 },
//     { label: 'Casablanca', year: 1942 },
//     { label: 'City Lights', year: 1931 },
//     { label: 'Psycho', year: 1960 },
//     { label: 'The Green Mile', year: 1999 },
//     { label: 'The Intouchables', year: 2011 },
//     { label: 'Modern Times', year: 1936 },
//     { label: 'Raiders of the Lost Ark', year: 1981 },
//     { label: 'Rear Window', year: 1954 },
//     { label: 'The Pianist', year: 2002 },
//     { label: 'The Departed', year: 2006 },
//     { label: 'Terminator 2: Judgment Day', year: 1991 },
//     { label: 'Back to the Future', year: 1985 },
//     { label: 'Whiplash', year: 2014 },
//     { label: 'Gladiator', year: 2000 },
//     { label: 'Memento', year: 2000 },
//     { label: 'The Prestige', year: 2006 },
//     { label: 'The Lion King', year: 1994 },
//     { label: 'Apocalypse Now', year: 1979 },
//     { label: 'Alien', year: 1979 },
//     { label: 'Sunset Boulevard', year: 1950 },
//     {
//         label: 'Dr. Strangelove or: How I Learned to Stop Worrying and Love the Bomb',
//         year: 1964,
//     },
//     { label: 'The Great Dictator', year: 1940 },
//     { label: 'Cinema Paradiso', year: 1988 },
//     { label: 'The Lives of Others', year: 2006 },
//     { label: 'Grave of the Fireflies', year: 1988 },
//     { label: 'Paths of Glory', year: 1957 },
//     { label: 'Django Unchained', year: 2012 },
//     { label: 'The Shining', year: 1980 },
//     { label: 'WALL·E', year: 2008 },
//     { label: 'American Beauty', year: 1999 },
//     { label: 'The Dark Knight Rises', year: 2012 },
//     { label: 'Princess Mononoke', year: 1997 },
//     { label: 'Aliens', year: 1986 },
//     { label: 'Oldboy', year: 2003 },
//     { label: 'Once Upon a Time in America', year: 1984 },
//     { label: 'Witness for the Prosecution', year: 1957 },
//     { label: 'Das Boot', year: 1981 },
//     { label: 'Citizen Kane', year: 1941 },
//     { label: 'North by Northwest', year: 1959 },
//     { label: 'Vertigo', year: 1958 },
//     {
//         label: 'Star Wars: Episode VI - Return of the Jedi',
//         year: 1983,
//     },
//     { label: 'Reservoir Dogs', year: 1992 },
//     { label: 'Braveheart', year: 1995 },
//     { label: 'M', year: 1931 },
//     { label: 'Requiem for a Dream', year: 2000 },
//     { label: 'Amélie', year: 2001 },
//     { label: 'A Clockwork Orange', year: 1971 },
//     { label: 'Like Stars on Earth', year: 2007 },
//     { label: 'Taxi Driver', year: 1976 },
//     { label: 'Lawrence of Arabia', year: 1962 },
//     { label: 'Double Indemnity', year: 1944 },
//     {
//         label: 'Eternal Sunshine of the Spotless Mind',
//         year: 2004,
//     },
//     { label: 'Amadeus', year: 1984 },
//     { label: 'To Kill a Mockingbird', year: 1962 },
//     { label: 'Toy Story 3', year: 2010 },
//     { label: 'Logan', year: 2017 },
//     { label: 'Full Metal Jacket', year: 1987 },
//     { label: 'Dangal', year: 2016 },
//     { label: 'The Sting', year: 1973 },
//     { label: '2001: A Space Odyssey', year: 1968 },
//     { label: "Singin' in the Rain", year: 1952 },
//     { label: 'Toy Story', year: 1995 },
//     { label: 'Bicycle Thieves', year: 1948 },
//     { label: 'The Kid', year: 1921 },
//     { label: 'Inglourious Basterds', year: 2009 },
//     { label: 'Snatch', year: 2000 },
//     { label: '3 Idiots', year: 2009 },
//     { label: 'Monty Python and the Holy Grail', year: 1975 },
// ];

export default Detail
