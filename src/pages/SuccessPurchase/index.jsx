import React from 'react'
import succesTransaction from '../../assets/EmailConfirm.png'
import { Typography, Button } from '@mui/material'
import HomeIcon from '@mui/icons-material/Home';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import { useNavigate } from 'react-router-dom';

const SuccessPurcase = () => {
    const navigate = useNavigate()

    const handleHome = () => {
        navigate('/')
    }

    const handleOpenInvoice = () => {
        navigate('/invoice')
    }

    return (
        <div>
            <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
                <img src={succesTransaction} alt="." style={{ marginTop: '54px' }} />
                <Typography style={{ marginTop: '44px', color: '#226957', fontSize: '24px' }}>Purchase Successfully</Typography>
                <Typography style={{ marginTop: '8px', fontSize: '16px', color: '#4F4F4F' }}>Thanks to buy a course! See u in the class</Typography>
                <div>
                    <Button
                        onClick={handleHome}
                        startIcon={<HomeIcon style={{ color: '#FFFFFF' }} />}
                        style={{ marginTop: '40px', background: '#EA9E1F', width: '200px', height: '50px', padding: '16px 24px', borderRadius: '6px', color: '#ffffff' }}>
                        <Typography style={{ fontSize: '15px', color: '#FFFFFF' }}>Back to Home</Typography>
                    </Button>
                    <Button
                        onClick={handleOpenInvoice}
                        startIcon={<ArrowForwardIcon style={{ color: '#FFFFFF' }} />}
                        style={{ marginTop: '40px', marginLeft: '24px', background: '#226957', width: '200px', height: '50px', padding: '16px 24px', borderRadius: '6px', color: '#ffffff' }}>
                        <Typography style={{ fontSize: '15px', color: '#FFFFFF' }}>
                            Open Invoice
                        </Typography>
                    </Button>
                </div>
            </div>
        </div>
    )
}

export default SuccessPurcase
