import { Box, Grid, Typography } from "@mui/material";
import Axios from "axios";
import React, { useEffect, useState } from "react";
import UseAuth from "../../hooks/UseAuth";
import { useNavigate } from "react-router-dom";


export default function MyClass() {
    // const course = useContext(itemContext)
    // const num = 4
    // const filt = course.filter(mycourse => mycourse.id === parseInt(num))
    const { apiUrl, userId, token } = UseAuth()
    const [myClass, setMyClass] = useState([])
    const navigate = useNavigate()

    // Axios.get(`${apiUrl}/invoice/getmyclass?user_id=${userId}`).then((res) => {
    //     setMyClass(res.data)
    // }).catch((error) => console.log(error))

    useEffect(() => {
        let config = {
            method: 'get',
            maxBodyLength: Infinity,
            url: `${apiUrl}/invoice/getmyclass?user_id=${userId}`,
            headers: {
                'Authorization': `Bearer ${token}`
            }
        }

        Axios.request(config)
            .then((response) => {
                setMyClass(response.data)
            })
            .catch((error) => {
                alert(error)
                navigate('/')
            })
    })

    return (
        <Grid
            container
            spacing={0}
            direction="column"
            alignItems="center"
            sx={{
                width: "100%",
                minHeight: "800px",
            }}
        >
            {myClass.map(my => {
                return (
                    <Box
                        sx={{
                            borderBottom: 1,
                            borderColor: 'grey.500',
                            display: 'flex',
                            flexDirection: 'row',
                            justifyContent: 'flex-start',
                            alignItems: 'center',
                            width: "70%",
                            minHeight: "20px",
                            marginTop: "20px",
                            paddingBottom: "20px"
                        }}
                    >
                        <img
                            src={my.course_image}
                            alt={my.course_name}
                            width="200px"
                        />
                        <Box sx={{ mx: '20px', display: 'flex', flexDirection: 'column', alignItems: 'flex-start' }}>
                            <Typography
                                gutterBottom
                                variant="span"
                                color="text.secondary"
                                component="div"
                                marginBottom="10px"
                            >
                                {my.course_type}
                            </Typography>
                            <Typography
                                variant="h6"
                                color="text.primary"
                                marginBottom="10px"
                                sx={{ fontWeight: 600 }}
                            >
                                {my.course_name}
                            </Typography>
                            <Typography
                                variant="h6"
                                sx={{ fontWeight: 500, fontSize: '20px', color: '#FABC1D' }}
                            >
                                {"Schedule : " + my.course_date}
                            </Typography>
                        </Box>
                    </Box>
                )
            })}
        </Grid>
    )
}
