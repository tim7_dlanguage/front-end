import React, { useEffect, useState } from 'react';
import {
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Paper,
    Button,
    Modal,
    TextField,
    styled,
    tableCellClasses,
    Switch,
    Box,
    Typography,
} from '@mui/material';
import Axios from 'axios';
import UseAuth from '../../hooks/UseAuth';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: "#226957",
        color: theme.palette.common.white,
        fontWeight: 700,
        fontSize: 16,
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 16,
        height: 55,
        width: 100
    },
}));

const StyledTableRow = styled(TableRow)(() => ({
    '&:nth-of-type(even)': {
        backgroundColor: '#EA9E1F33',
    },
    '&:last-child td, &:last-child th': {
        border: 0,
    },
}));

const User = () => {
    const { apiUrl, token } = UseAuth()
    const [userList, setUserList] = useState([])
    const [open, setOpen] = useState(false);
    const [add, setAdd] = useState(false)
    const [modalData, setModalData] = useState({})
    const [pass, setPass] = useState('user2023')

    const [addUser, setAddUser] = useState({
        name: '',
        email: '',
        password: '',
        role: '',
        status: true
    })

    useEffect(() => {
        let config = {
            method: 'get',
            maxBodyLength: Infinity,
            url: `${apiUrl}/User/User`,
            headers: {
                'Authorization': `Bearer ${token}`
            }
        }

        Axios.request(config)
            .then((response) => {
                setUserList(response.data)
                //console.log(userList)
            })
            .catch((error) => {
                console.log(error)
            })
    })

    const handleClose = () => {
        setOpen(false);
    };

    const handleAdd = () => {
        setAdd(true)
    }

    const handleCloseAdd = () => {
        setAdd(false)
    }

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setModalData({ ...modalData, [name]: value });
    };

    const handleAddChange = (e) => {
        const { name, value } = e.target;
        setAddUser({ ...addUser, [name]: value });
    }

    const handleAddUser = () => {
        let data = JSON.stringify({
            "id": '3fa85f64-5717-4562-b3fc-2c963f66afa6',
            "name": addUser.name,
            "email": addUser.email,
            "password": addUser.password,
            "status": addUser.status,
            "role": addUser.role
        })

        let config = {
            method: 'post',
            maxBodyLength: Infinity,
            url: `${apiUrl}/User/User`,
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            },
            data: data
        }

        Axios.request(config)
            .then((response) => {
                alert(response.data)
            })
            .catch((error) => {
                alert(error.response.data);
                console.log(error.response.data)
            })

        console.log(data)

        handleCloseAdd();
    };

    const handleEditUser = () => {
        let data = JSON.stringify({
            "id": modalData.id,
            "name": modalData.name,
            "email": modalData.email,
            "password": pass,
            "status": modalData.status,
            "role": modalData.role
        })

        let config = {
            method: 'put',
            maxBodyLength: Infinity,
            url: `${apiUrl}/User/User`,
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            },
            data: data
        }

        Axios.request(config)
            .then((response) => {
                alert(response.data)
            })
            .catch((error) => {
                alert(error.response.data);
            })

        handleClose();
    };

    function handleDeleteUser(user_id) {
        let config = {
            method: 'delete',
            maxBodyLength: Infinity,
            url: `${apiUrl}/User/User?user_id=${user_id}`,
            headers: {
                'Authorization': `Bearer ${token}`
            }
        }

        Axios.request(config)
            .then((response) => {
                console.log(response.data)
            })
            .catch((error) => {
                alert(error)
            })

    }

    return (
        <div>
            <Button variant="contained" color="primary" onClick={handleAdd}>
                Add User
            </Button>
            <TableContainer component={Paper} sx={{ boxShadow: "none", maxWidth: 1000, mx: "auto" }}>
                <Table aria-label="customized table">
                    <TableHead>
                        <TableRow>
                            <StyledTableCell align='center'>Name</StyledTableCell>
                            <StyledTableCell align='center'>Email</StyledTableCell>
                            <StyledTableCell align='center'>Role</StyledTableCell>
                            <StyledTableCell align='center'>Active</StyledTableCell>
                            <StyledTableCell align='center'>Action</StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {userList.map((c) => (
                            <StyledTableRow key={c.id}>
                                <StyledTableCell align='center'>{c.name}</StyledTableCell>
                                <StyledTableCell align='center'>{c.email}</StyledTableCell>
                                <StyledTableCell align='center'>{c.role}</StyledTableCell>
                                <StyledTableCell align='center'>{c.status ? "Active" : "Inactive"}</StyledTableCell>
                                <StyledTableCell align='center'>
                                    <Button variant='contained' component='div' sx={{ my: '0.2vh' }}
                                        style={{
                                            borderRadius: '8px',
                                            backgroundColor: '#226957',
                                            color: '#FFFFFF',
                                            width: '80px',
                                            height: '30px',
                                        }}
                                        onClick={() => {
                                            setOpen(true)
                                            setModalData(c)
                                        }}
                                    >Edit</Button>
                                    <Button variant='contained' component='div' sx={{ my: '0.2vh' }}
                                        style={{
                                            borderRadius: '8px',
                                            backgroundColor: '#DC143C',
                                            color: '#FFFFFF',
                                            width: '80px',
                                            height: '30px'
                                        }}
                                        onClick={() => handleDeleteUser(c.id)}
                                    >Delete</Button></StyledTableCell>
                            </StyledTableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
            <Modal open={add} onClose={handleCloseAdd}>
                <div style={{ padding: '20px', width: '300px', margin: '100px auto', backgroundColor: '#fff' }}>
                    <h2>Add User</h2>
                    <TextField
                        label="Name"
                        name="name"
                        value={addUser.name}
                        onChange={handleAddChange}
                        margin="normal"
                        fullWidth
                    />
                    <TextField
                        label="Email"
                        name="email"
                        value={addUser.email}
                        onChange={handleAddChange}
                        margin="normal"
                        fullWidth
                    />
                    <TextField
                        label="Password"
                        name="password"
                        type='password'
                        value={addUser.password}
                        onChange={handleAddChange}
                        margin="normal"
                        fullWidth
                    />
                    <TextField
                        label="Role"
                        name="role"
                        value={addUser.role}
                        onChange={handleAddChange}
                        margin="normal"
                        fullWidth
                    />
                    <Box sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
                        <Typography>
                            Active ?
                        </Typography>
                        <Switch checked={addUser.status} onChange={(e) => {
                            setAddUser({ ...addUser, status: e.target.checked })
                        }} />
                    </Box>
                    <Button variant="contained" color="primary" onClick={handleAddUser}>
                        Add
                    </Button>
                </div>
            </Modal>
            <Modal open={open} onClose={handleClose}>
                <div style={{ padding: '20px', width: '300px', margin: '100px auto', backgroundColor: '#fff' }}>
                    <h2>Edit User</h2>
                    <TextField
                        label="Name"
                        name="name"
                        value={modalData.name}
                        onChange={handleInputChange}
                        margin="normal"
                        fullWidth
                    />
                    <TextField
                        label="Email"
                        name="email"
                        value={modalData.email}
                        onChange={handleInputChange}
                        margin="normal"
                        fullWidth
                    />
                    <TextField
                        label="Password"
                        name="password"
                        type='password'
                        value={pass}
                        onChange={(e) => setPass(e.target.value)}
                        margin="normal"
                        fullWidth
                    />
                    <TextField
                        label="Role"
                        name="role"
                        value={modalData.role}
                        onChange={handleInputChange}
                        margin="normal"
                        fullWidth
                    />
                    <Box sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
                        <Typography>
                            Active ?
                        </Typography>
                        <Switch checked={modalData.status} onChange={(e) => {
                            setModalData({ ...modalData, status: e.target.checked })
                        }} />
                    </Box>
                    <Button variant="contained" color="primary" onClick={handleEditUser}>
                        Edit
                    </Button>
                </div>
            </Modal>
        </div>
    );
};

export default User;
