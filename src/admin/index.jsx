import React from 'react';
import { Box, Button } from '@mui/material';
import { useNavigate } from 'react-router-dom';

const CrudComponent = () => {
    const navigate = useNavigate()

    const handleCourse = () => {
        navigate('/admin/course')
    }

    const handleCategory = () => {
        navigate('/admin/category')
    }

    const handleUser = () => {
        navigate('/admin/user')
    }

    const handlePaymentMethod = () => {
        navigate('/admin/paymentmethod')
    }

    const handleInvoice = () => {
        navigate('/admin/invoice')
    }

    return (
        <div>
            <Box sx={{display:'flex',flexDirection:'column',alignItems:'center', gap:'1vw'}}>
                <Button variant="contained" component="div"
                    style={{
                        borderRadius: '8px',
                        backgroundColor: '#226957',
                        color: '#FFFFFF',
                        width: '175px',
                        height: '40px'
                    }}
                    onClick={handleCourse}>
                    Course
                </Button>
                <Button variant="contained" component="div"
                    style={{
                        borderRadius: '8px',
                        backgroundColor: '#226957',
                        color: '#FFFFFF',
                        width: '175px',
                        height: '40px'
                    }}
                    onClick={handleCategory}>
                    Category
                </Button>
                <Button variant="contained" component="div"
                    style={{
                        borderRadius: '8px',
                        backgroundColor: '#226957',
                        color: '#FFFFFF',
                        width: '175px',
                        height: '40px'
                    }}
                    onClick={handleUser}>
                    User
                </Button>
                <Button variant="contained" component="div"
                    style={{
                        borderRadius: '8px',
                        backgroundColor: '#226957',
                        color: '#FFFFFF',
                        width: '175px',
                        height: '40px'
                    }}
                    onClick={handlePaymentMethod}>
                    Payment Method
                </Button>
                <Button variant="contained" component="div"
                    style={{
                        borderRadius: '8px',
                        backgroundColor: '#226957',
                        color: '#FFFFFF',
                        width: '175px',
                        height: '40px'
                    }}
                    onClick={handleInvoice}>
                    Invoice
                </Button>
            </Box>
        </div>
    );
};

export default CrudComponent;
