import React, { useEffect, useState } from 'react';
import {
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Paper,
    Button,
    Modal,
    TextField,
    styled,
    tableCellClasses,
    Switch,
    Box,
    Typography,
} from '@mui/material';
import Axios from 'axios';
import UseAuth from '../../hooks/UseAuth';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: "#226957",
        color: theme.palette.common.white,
        fontWeight: 700,
        fontSize: 16,
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 16,
        height: 55,
        width: 100
    },
}));

const StyledTableRow = styled(TableRow)(() => ({
    '&:nth-of-type(even)': {
        backgroundColor: '#EA9E1F33',
    },
    '&:last-child td, &:last-child th': {
        border: 0,
    },
}));

const PaymentMethod = () => {
    const { apiUrl, token } = UseAuth()
    const [paymentMethodList, setPaymentMethodList] = useState([])
    const [modalData, setModalData] = useState({})
    const [open, setOpen] = useState(false);
    const [add, setAdd] = useState(false)

    const [addPayment, setAddPayment] = useState({
        name: '',
        image: '',
        isActive: true,
    })

    useEffect(() => {
        let config = {
            method: 'get',
            maxBodyLength: Infinity,
            url: `${apiUrl}/Cart/PaymentMethod`,
            headers: {
                'Authorization': `Bearer ${token}`
            }
        }

        Axios.request(config)
            .then((response) => {
                setPaymentMethodList(response.data)
                //console.log(paymentMethodList)
            })
            .catch((error) => {
                console.log(error)
            })
    })

    const handleClose = () => {
        setOpen(false);
    };

    const handleAdd = () => {
        setAdd(true)
    }

    const handleCloseAdd = () => {
        setAdd(false)
    }

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setModalData({ ...modalData, [name]: value });
    };

    const handleAddChange = (e) => {
        const { name, value } = e.target;
        setAddPayment({ ...addPayment, [name]: value });
    }

    const handleAddPayment = () => {
        let data = JSON.stringify({
            "id": '0',
            "name": addPayment.name,
            "email": addPayment.email,
            "isActive": addPayment.isActive
        })

        let config = {
            method: 'post',
            maxBodyLength: Infinity,
            url: `${apiUrl}/Cart/PaymentMethod`,
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            },
            data: data
        }

        Axios.request(config)
            .then((response) => {
                alert(response.data)
            })
            .catch((error) => {
                alert(error.response.data);
                console.log(error.response.data)
            })

        console.log(data)

        handleCloseAdd();
    };

    const handleEditPayment = () => {
        let data = JSON.stringify({
            "id": modalData.id,
            "name": modalData.name,
            "image": modalData.image,
            "isActive": modalData.isActive
        })

        let config = {
            method: 'put',
            maxBodyLength: Infinity,
            url: `${apiUrl}/Cart/PaymentMethod`,
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            },
            data: data
        }

        Axios.request(config)
            .then((response) => {
                alert(response.data)
            })
            .catch((error) => {
                alert(error.response.data);
                console.log(error.response.data)
            })

        console.log(data)

        handleClose();
    };

    return (
        <div>
            <Button variant="contained" color="primary" onClick={handleAdd}>
                Add User
            </Button>
            <TableContainer component={Paper} sx={{ boxShadow: "none", maxWidth: 1000, mx: "auto" }}>
                <Table aria-label="customized table">
                    <TableHead>
                        <TableRow>
                            <StyledTableCell align='center'>No</StyledTableCell>
                            <StyledTableCell align='center'>Payment Logo</StyledTableCell>
                            <StyledTableCell align='center'>Payment Name</StyledTableCell>
                            <StyledTableCell align='center'>Active</StyledTableCell>
                            <StyledTableCell align='center'>Action</StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {paymentMethodList.map((c, index) => (
                            <StyledTableRow key={index}>
                                <StyledTableCell align='center'>{index + 1}</StyledTableCell>
                                <StyledTableCell align='center'><img src={c.image} alt='DLanguage' width='80px' /></StyledTableCell>
                                <StyledTableCell align='center'>{c.name}</StyledTableCell>
                                <StyledTableCell align='center'>{c.isActive ? "Active" : "Inactive"}</StyledTableCell>
                                <StyledTableCell align='center'>
                                    <Button variant='contained' component='div' sx={{ my: '0.2vh' }}
                                        style={{
                                            borderRadius: '8px',
                                            backgroundColor: '#226957',
                                            color: '#FFFFFF',
                                            width: '100px',
                                            height: '40px',
                                        }}
                                        onClick={() => {
                                            setOpen(true)
                                            setModalData(c)
                                        }}>
                                        Edit
                                    </Button>
                                </StyledTableCell>
                            </StyledTableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
            <Modal open={add} onClose={handleCloseAdd}>
                <div style={{ padding: '20px', width: '300px', margin: '100px auto', backgroundColor: '#fff' }}>
                    <h2>Add User</h2>
                    <TextField
                        label="Name"
                        name="name"
                        value={addPayment.name}
                        onChange={handleAddChange}
                        margin="normal"
                        fullWidth
                    />
                    <TextField
                        multiline
                        label="Image"
                        name="image"
                        value={addPayment.image}
                        onChange={handleAddChange}
                        margin="normal"
                        fullWidth
                    />
                    <Box sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
                        <Typography>
                            Active ?
                        </Typography>
                        <Switch checked={addPayment.status} onChange={(e) => {
                            setAddPayment({ ...addPayment, status: e.target.checked })
                        }} />
                    </Box>
                    <Button variant="contained" color="primary" onClick={handleAddPayment}>
                        Add
                    </Button>
                </div>
            </Modal>
            <Modal open={open} onClose={handleClose}>
                <div style={{ padding: '20px', width: '300px', margin: '100px auto', backgroundColor: '#fff' }}>
                    <h2>Edit User</h2>
                    <TextField
                        label="Name"
                        name="name"
                        value={modalData.name}
                        onChange={handleInputChange}
                        margin="normal"
                        fullWidth
                    />
                    <TextField
                        multiline
                        label="Image"
                        name="image"
                        value={modalData.image}
                        onChange={handleInputChange}
                        margin="normal"
                        fullWidth
                    />
                    <Box sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
                        <Typography>
                            Active ?
                        </Typography>
                        <Switch checked={modalData.isActive} onChange={(e) => {
                            setModalData({ ...modalData, isActive: e.target.checked })
                        }} />
                    </Box>
                    <Button variant="contained" color="primary" onClick={handleEditPayment}>
                        Edit
                    </Button>
                </div>
            </Modal>
        </div>
    );
};

export default PaymentMethod;
