import { useNavigate, useParams } from 'react-router-dom'
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Axios from 'axios';
import React, { useEffect, useState } from 'react'
import UseAuth from '../../hooks/UseAuth';
import { Box, Typography } from '@mui/material';


const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: "#226957",
    color: theme.palette.common.white,
    fontWeight: 700,
    fontSize: 16,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 16,
    height: 55,
    width: 100
  },
}));

const StyledTableRow = styled(TableRow)(() => ({
  '&:nth-of-type(even)': {
    backgroundColor: '#EA9E1F33',
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));

function InvDetail() {
  const { id } = useParams();
  const [Invoice, setInvoice] = useState({});
  const [invoiceDetail, setInvoiceDetail] = useState([]);
  const { apiUrl, token } = UseAuth();
  const navigate = useNavigate()

  useEffect(() => {
    let config = {
      method: 'get',
      maxBodyLength: Infinity,
      url: `${apiUrl}/Invoice/GetInvoiceDetail?invoice_id=${id}`,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }

    Axios.request(config)
      .then((response) => {
        setInvoice(response.data.invoice)
        setInvoiceDetail(response.data.courses)
      })
      .catch((error) => {
        alert(error)
        navigate('/')
      })
  })

  return (
    <div>
      <br />
      <br />
      <TableContainer component={Paper} sx={{ boxShadow: "none", maxWidth: 1400, mx: "auto" }}>
        <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-start' }}>
          <br />
          <Typography sx={{ fontSize: '20px', fontWeight: '600' }}>
            Details Invoice
          </Typography>
          <br />
          <Typography>
            {"No. Invoice: " + id}
          </Typography>
          <Box sx={{ width: '100%', display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
            <Typography>
              {"Date : " + Invoice.invoice_date}
            </Typography>
            <Typography sx={{ fontSize: '18px', fontWeight: '700' }}>
              {"Total Price IDR " + parseInt(Invoice.total_price).toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')}
            </Typography>
          </Box>
          <br />
        </Box>
        <Table aria-label="customized table">
          <TableHead>
            <TableRow>
              <StyledTableCell align='center'>No</StyledTableCell>
              <StyledTableCell align='center'>Course Name</StyledTableCell>
              <StyledTableCell align='center'>Language</StyledTableCell>
              <StyledTableCell align='center'>Schedule</StyledTableCell>
              <StyledTableCell align='center'>Price</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {invoiceDetail.map((row, index) => (
              <StyledTableRow key={index}>
                <StyledTableCell component="th" scope="row" align='center'>
                  {index + 1}
                </StyledTableCell>
                <StyledTableCell align='center'>{row.course.name}</StyledTableCell>
                <StyledTableCell align='center'>{row.course.type}</StyledTableCell>
                <StyledTableCell align='center'>{row.course_date}</StyledTableCell>
                <StyledTableCell align='center'>{"IDR " + parseInt(row.course.price).toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')}</StyledTableCell>
              </StyledTableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>

  );
}

export default InvDetail;
