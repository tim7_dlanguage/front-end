import React, { useEffect, useState } from 'react';
import {
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Paper,
    Button,
    styled,
    tableCellClasses,
} from '@mui/material';
import Axios from 'axios';
import UseAuth from '../../hooks/UseAuth';
import { useNavigate } from 'react-router-dom';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: "#226957",
        color: theme.palette.common.white,
        fontWeight: 700,
        fontSize: 16,
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 16,
        height: 55,
        width: 100
    },
}));

const StyledTableRow = styled(TableRow)(() => ({
    '&:nth-of-type(even)': {
        backgroundColor: '#EA9E1F33',
    },
    '&:last-child td, &:last-child th': {
        border: 0,
    },
}));

const User = () => {
    const { apiUrl, token } = UseAuth()
    const [invoiceList, setInvoiceList] = useState([])
    const navigate = useNavigate()

    useEffect(() => {
        let config = {
            method: 'get',
            maxBodyLength: Infinity,
            url: `${apiUrl}/Invoice/Invoice`,
            headers: {
                'Authorization': `Bearer ${token}`
            }
        }

        Axios.request(config)
            .then((response) => {
                setInvoiceList(response.data)
                //console.log(invoiceList)
            })
            .catch((error) => {
                console.log(error)
            })
    })

    function handleDetail(invoice_id) {
        navigate(`/admin/invoice/detail/${invoice_id}`)
    }

    return (
        <div>
            <TableContainer component={Paper} sx={{ boxShadow: "none", maxWidth: 1000, mx: "auto" }}>
                <Table aria-label="customized table">
                    <TableHead>
                        <TableRow>
                            <StyledTableCell align='center'>No. Invoice</StyledTableCell>
                            <StyledTableCell align='center'>Invoice Date</StyledTableCell>
                            <StyledTableCell align='center'>Total Course</StyledTableCell>
                            <StyledTableCell align='center'>Total Price</StyledTableCell>
                            <StyledTableCell align='center'>Action</StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {invoiceList.map((c, index) => (
                            <StyledTableRow key={c.id}>
                                <StyledTableCell align='center'>{c.invoice_id}</StyledTableCell>
                                <StyledTableCell align='center'>{c.invoice_date}</StyledTableCell>
                                <StyledTableCell align='center'>{c.total_course}</StyledTableCell>
                                <StyledTableCell align='center'>{"Rp. " + c.total_price.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')}</StyledTableCell>
                                <StyledTableCell align='center'>
                                    <Button variant='contained' component='div' sx={{ my: '0.2vh' }}
                                        style={{
                                            borderRadius: '8px',
                                            backgroundColor: '#226957',
                                            color: '#FFFFFF',
                                            width: '100px',
                                            height: '40px',
                                        }}
                                        onClick={() => handleDetail(c.invoice_id)}>
                                        Details
                                    </Button>
                                </StyledTableCell>
                            </StyledTableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    );
};

export default User;
