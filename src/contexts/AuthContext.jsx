import Axios from "axios";
import { createContext, useState } from "react";

export const AuthContext = createContext(null)

export const AuthProvider = ({ children }) => {
    const [token, setToken] = useState('')
    const [userId, setUserId] = useState('')
    const [userName, setUserName] = useState('')
    const [userEmail, setUserEmail] = useState('')
    const [userRole, setUserRole] = useState('')
    const apiUrl = process.env.REACT_APP_API_URL

    Axios.get(`${apiUrl}/User/Verify?token=${token}`).then((res) => {
        setUserId(res.data.id)
        setUserName(res.data.name)
        setUserEmail(res.data.email)
        setUserRole(res.data.role)
    }).catch(() => {
        setUserId('')
        setUserName('')
        setUserEmail('')
        setUserRole('')

        console.clear()
    })



    return (
        <AuthContext.Provider value={{
            apiUrl,
            token,
            setToken,
            userId,
            userName,
            userEmail,
            userRole
        }}>
            {children}
        </AuthContext.Provider>
    )
}