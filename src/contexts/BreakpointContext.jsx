import { useMediaQuery } from "@mui/material";
import { createContext } from "react";

export const BreakpointhContext = createContext(null)

export const BreakpointProvider = ({ children }) => {
    const mobileWidth = '600px'
    const isMobile = useMediaQuery(`(max-width: ${mobileWidth})`)

    return (
        <BreakpointhContext.Provider value={{
            mobileWidth,
            isMobile
        }}>
            {children}
        </BreakpointhContext.Provider>
    )
}