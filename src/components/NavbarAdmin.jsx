import '../components/Navbar.css';
import {Outlet, Link } from "react-router-dom";
import LogoutIcon from '@mui/icons-material/Logout';
import Button from '@mui/material/Button';  
import UseAuth from '../hooks/UseAuth';

export default function NavbarAdmin(){
    const { isLoggedIn } = UseAuth();
    
    return(
        <div>
            <div className="container">
            <img className="logo" src={process.env.PUBLIC_URL + "/Logo.svg"} alt="Logo DLanguage"/>
            
                    <div className="btnContainer">
                    <Link to="/Admin/category" style={{ color: "#226957", marginRight: 20, textDecoration: "none" }}>
                        Category
                    </Link>
                    <Link to="/Admin/course" style={{ color: "#226957", marginRight: 20, textDecoration: "none" }}>
                        Course
                    </Link>
                    <Link to="/Admin/user" style={{ color: "#226957", marginRight: 20, textDecoration: "none" }}>
                        User
                    </Link>
                    <Link to="/Admin/method" style={{ color: "#226957", marginRight: 20, textDecoration: "none" }}>
                        Method
                    </Link>
                    <Link to="/Admin/invoice" style={{ color: "#226957", marginRight: 20, textDecoration: "none" }}>
                        Invoice
                    </Link>
                    <Link to="/Admin">
                        <Button variant="text" style={{ color: "red", marginRight: 20 }}><LogoutIcon /></Button>
                    </Link>
                </div>
                
            </div>

            <Outlet/>
        </div>
    );
}
