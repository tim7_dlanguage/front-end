import { Grid } from '@mui/material'
import Axios from 'axios'
import React, { useContext, useEffect, useState } from 'react'
import Product from '../ProductCard'
import { AuthContext } from '../../../contexts/AuthContext'

const ListProduct = ({ productType }) => {
    const { apiUrl } = useContext(AuthContext)
    const [data, setData] = useState([])

    useEffect(() => {
        Axios.get(`${apiUrl}/Course/getcourseallActive`).then(res => {
            setData(productType === '' ? res.data : res.data.filter(function (x) {
                return x.type === productType
            }))
        }).catch((error)=>console.log(error))
    })

    return (
        <div>
            <Grid container justifyContent='center' gap={3} sx={{ maxWidth: '1200px', mx: 'auto' }}>
                {data.map((item, index) => (
                    <Product key={index} item
                        id={item.id} 
                        image={item.image}
                        type={item.type}
                        name={item.name}
                        price={item.price} />
                ))}
            </Grid>
        </div>
    )
}

export default ListProduct
