import { Card, CardActionArea, CardContent, CardMedia, Grid, Typography } from '@mui/material'
import React from 'react'
import { useNavigate } from 'react-router-dom'
import PropTypes from 'prop-types'

const ProductCard = ({ id, image, type, name, price }) => {
    const navigate = useNavigate()

    function openDetailById(productId) {
        navigate(`/detail/${productId}`)
    }

    return (
        <Grid>
            <Card sx={{
                width: '350px',
            }}>
                <CardActionArea onClick={() => openDetailById(id)}>
                    <CardMedia
                        component='img'
                        height="140"
                        image={image}
                        alt='soup'
                        sx={{ height: '200px' }} />
                    <CardContent sx={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-start' }}>
                        <Typography sx={{
                            color: '#828282',
                            fontSize: '12px',
                            fontWeight: '400'
                        }}>
                            {type}
                        </Typography>
                        <Typography sx={{
                            color: '#5B4947',
                            fontSize: '16px',
                            fontWeight: '600',
                            height: 'auto'
                        }}>
                            {name}
                        </Typography>
                        <Typography sx={{
                            color: '#226957',
                            fontSize: '16px',
                            fontWeight: '600'
                        }}>
                            {price}
                        </Typography>
                    </CardContent>
                </CardActionArea>
            </Card>
        </Grid>
    )
}

ProductCard.propTypes = {
    id: PropTypes.number.isRequired,
    image: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired
}

export default ProductCard
