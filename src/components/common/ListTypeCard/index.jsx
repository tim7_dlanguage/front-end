import { Card, CardActionArea, CardContent, CardMedia, Grid, Typography } from '@mui/material'
import React from 'react'
import { useNavigate } from 'react-router-dom'
import PropTypes from 'prop-types'

const ProductCard = ({ type_id, type_name, type_image }) => {
    const navigate = useNavigate()

    function openDetailById(type_id) {
        navigate(`/menu/${type_id}`)
    }

    return (
        <Grid>
            <Card sx={{
                width: '250px',
            }}>
                <CardActionArea onClick={() => openDetailById(type_id)}>
                    <CardMedia
                        component='img'
                        height="140"
                        image={type_image}
                        alt='soup'
                        sx={{ height:'150px' }} />
                    <CardContent sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                        <Typography sx={{
                            color: '#828282',
                            fontSize: '24px',
                            fontWeight: '400'
                        }}>
                            {type_name}
                        </Typography>
                    </CardContent>
                </CardActionArea>
            </Card>
        </Grid>
    )
}

ProductCard.propTypes = {
    id: PropTypes.number.isRequired,
    image: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired
}

export default ProductCard
