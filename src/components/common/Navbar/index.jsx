import React, { useContext } from 'react'
import { Outlet, useNavigate } from 'react-router-dom'
import '../../index.css'
import Logo from '../../../assets/Logo.svg';
import MenuRoundedIcon from '@mui/icons-material/MenuRounded';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import PersonIcon from '@mui/icons-material/Person';
import LogoutIcon from '@mui/icons-material/Logout';
import { AppBar, Box, Button, IconButton, Toolbar, Typography } from '@mui/material';
import UseAuth from '../../../hooks/UseAuth';
import { BreakpointhContext } from '../../../contexts/BreakpointContext';
import Popup from 'reactjs-popup';


const iconMargin = {
  flexGrow: 1,
  mx: 2,
  fontSize: '16px',
  color: '#226957',
  fontWeight: '500',
  textTransform: 'none'
}

const Navbar = () => {
  const navigate = useNavigate()
  const { token, setToken, userRole } = UseAuth()
  const { mobileWidth, isMobile } = useContext(BreakpointhContext)

  // let data = JSON.stringify({
  //   "email": "syahid.prabowo12@gmail.com",
  //   "password": "12345678"
  // })

  // let config = {
  //   method: 'post',
  //   maxBodyLength: Infinity,
  //   url: `${apiUrl}/User/Login`,
  //   headers: {
  //     'Content-Type': 'application/json'
  //   },
  //   data: data
  // }

  const handleLogin = () => {
    navigate('/login')
    // Axios.request(config)
    //   .then((response) => {
    //     setToken(response.data)
    //   })
    //   .catch((error) => {
    //     console.log(error);
    //   });
  }

  const handleLogout = () => {
    setToken('')
  }

  const handleRegister = () => {
    navigate('/register')
  }

  const handleCart = () => {
    navigate('/checkout')
  }

  const handleMyClass = () => {
    navigate('/myclass')
  }

  const handleInvoice = () => {
    navigate('/invoice')
  }

  const handleAdmin = () => {
    navigate('/admin')
  }

  return (
    <div>
      <AppBar position='static'>
        <Toolbar sx={{
          backgroundColor: 'white',
          color: 'black',
          display: 'flex',
          justifyContent: 'space-between',
          minWidth: isMobile ? 'none' : mobileWidth
        }}>
          <Box sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
            <Button style={{ textDecoration: 'none' }} onClick={() => navigate('/')}>
              <img src={Logo} alt="" width={isMobile ? '30px' : '45px'} />
              <Typography sx={{ textTransform: 'capitalize', color: 'black', fontSize: isMobile ? '16px' : '24px', fontWeight: '400', ml: '10px' }}>
                Language
              </Typography>
            </Button>
          </Box>

          {isMobile ?
            <Popup
              trigger={<MenuRoundedIcon />}
              position='bottom right'>
              {token !== '' ?
                <Box sx={{ display: 'flex', flexDirection: 'column' }}>
                  <Button onClick={handleLogout}>Logout</Button>
                </Box>
                :
                <Box sx={{ display: 'flex', flexDirection: 'column' }}>
                  <Button onClick={handleLogin}>Login</Button>
                  <Button onClick={handleLogin}>Register</Button>
                </Box>
              }
            </Popup>
            :
            token !== '' ?
              <Box sx={{
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center',
                justifyItems: 'flex-end'
              }}>
                <IconButton
                  size="large"
                  edge="start"
                  aria-label="menu"
                  sx={iconMargin}
                  onClick={handleCart}
                >
                  <ShoppingCartIcon />
                </IconButton>
                <Button variant="h7" component="div" sx={iconMargin} onClick={handleMyClass}>
                  MyClass
                </Button>
                <Button variant="h7" component="div" sx={iconMargin} onClick={handleInvoice}>
                  Invoice
                </Button>
                {userRole === 'admin' ?
                  <IconButton
                    size="large"
                    edge="start"
                    aria-label="menu"
                    sx={iconMargin}
                    style={{ color: '#226957' }}
                    onClick={handleAdmin}
                  >
                    <PersonIcon />
                  </IconButton>
                  :
                  <></>
                }

                <Typography variant="h5" component="div" sx={iconMargin}>
                  <hr style={{
                    borderColor: 'black',
                    width: '20px',
                    rotate: '90deg'
                  }} />
                </Typography>
                <IconButton
                  size="large"
                  edge="start"
                  color="inherit"
                  aria-label="menu"
                  sx={iconMargin}
                  onClick={handleLogout}
                >
                  <LogoutIcon />
                </IconButton>
              </Box>
              :
              <Box sx={{
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center',
                justifyItems: 'flex-end'
              }}>
                <Button variant="contained" component="div" sx={iconMargin}
                  style={{
                    borderRadius: '8px',
                    backgroundColor: '#226957',
                    color: '#FFFFFF',
                    width: '175px',
                    height: '40px'
                  }}
                  onClick={handleLogin}>
                  Login
                </Button>
                <Button variant="contained" component="div" sx={iconMargin}
                  style={{
                    borderRadius: '8px',
                    backgroundColor: '#EA9E1F',
                    color: '#FFFFFF',
                    width: '175px',
                    height: '40px'
                  }}
                  onClick={handleRegister}>
                  Register
                </Button>
              </Box>
          }
        </Toolbar>
      </AppBar>
      <Outlet />
    </div>
  )
}

export default Navbar