import { Grid } from '@mui/material'
import Axios from 'axios'
import React, { useContext, useEffect, useState } from 'react'
import ListTypeCard from '../ListTypeCard'
import { AuthContext } from '../../../contexts/AuthContext'

const ListType = ({ productType }) => {
    const { apiUrl } = useContext(AuthContext)
    const [data, setData] = useState([])

    useEffect(() => {
        Axios.get(`${apiUrl}/CourseType/getcoursetypeallActive`).then(res => {
            setData(productType === '' ? res.data : res.data.filter(function (x) {
                return x.type === productType
            }))
        }).catch((error)=>console.log(error))
    })

    return (
        <div>
            <Grid container justifyContent='center' gap={3} sx={{ maxWidth: '1200px', mx: 'auto' }}>
                {data.map((item, index) => (
                    <ListTypeCard key={index} item
                        type_id={item.type_id} 
                        type_image={item.type_image}
                        type_name={item.type_name} />
                    ))}
            </Grid>
        </div>
    )
}

export default ListType
