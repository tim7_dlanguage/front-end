import React, { useEffect, useState } from 'react';
import { Dialog, DialogActions, Button, Typography, Box } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import Axios from 'axios';
import UseAuth from '../../../hooks/UseAuth';

const styles = {
    paymentMethodBox: {
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        marginBottom: "16px",
        cursor: "pointer",
    },
    paymentMethodLogo: {
        width: "40px",
        height: "40px",
        marginLeft: "10px",
        marginRight: "20px"
    },
    paymentMethodName: {
        fontFamily: "Poppins",
        fontSize: "18px",
        fontWeight: 500,

    },
};

const PaymentModal = ({ openDialog, handleCloseDialog, selectedProductsId }) => {
    const { apiUrl, userId } = UseAuth()
    const [paymentMethods, setPaymentMethods] = useState([])
    const navigate = useNavigate()

    useEffect(() => {
        Axios.get(`${apiUrl}/Cart/GetPaymentMethod`).then((res) => setPaymentMethods(res.data)).catch((error) => console.log(error.response.data))
    })

    const handlePaymentButtonClick = () => {
        let data = JSON.stringify(selectedProductsId);

        let config = {
            method: 'put',
            maxBodyLength: Infinity,
            url: `${apiUrl}/Invoice/Checkout?user_id=${userId}`,
            headers: {
                'Content-Type': 'application/json'
            },
            data: data
        };

        Axios.request(config)
            .then((response) => {
                console.log(response.data)
            })
            .catch((error) => {
                console.log(error)
            });

        navigate('/successpurchase')
    }

    return (
        <Dialog open={openDialog} onClose={handleCloseDialog} >
            <Box
                sx={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    margin: "0 auto",
                    width: "374px",
                    padding: "20px",
                    backgroundColor: "#fff",
                    borderRadius: "10px",
                    textAlign: "left", // Align all content to the left
                }}
            >
                <Typography
                    sx={{
                        textAlign: "center",
                        fontFamily: "Poppins",
                        fontSize: "20px",
                        fontWeight: 500,
                        marginBottom: "24px",
                        marginLeft: 0, // Reset marginLeft to 0
                    }}
                >
                    Select Payment Method
                </Typography>

                {/* Render payment methods dynamically */}
                {paymentMethods.map((method) => (
                    <Box
                        key={method.id}
                        sx={styles.paymentMethodBox}
                    >
                        <img
                            src={method.image}
                            alt={`${method.name} Logo`}
                            style={styles.paymentMethodLogo}
                        />
                        <Typography
                            style={styles.paymentMethodName}
                        >
                            {method.name}
                        </Typography>
                    </Box>
                ))}

                {/* Add payment form or payment details here */}
                {/* You can implement the payment form or integrate a payment gateway */}

                <DialogActions>
                    <Button
                        variant='contained'
                        style={{
                            borderRadius: '8px',
                            backgroundColor: '#226957',
                            color: '#FFFFFF',
                            width: '175px',
                            height: '40px'
                        }}
                        onClick={handleCloseDialog}
                    >
                        Close
                    </Button>
                    <Button
                        variant="contained"
                        style={{
                            borderRadius: '8px',
                            backgroundColor: '#EA9E1F',
                            color: '#FFFFFF',
                            width: '175px',
                            height: '40px'
                        }}
                        onClick={handlePaymentButtonClick}
                    >
                        Pay Now
                    </Button>
                </DialogActions>
            </Box>
        </Dialog>
    )
}

export default PaymentModal;
