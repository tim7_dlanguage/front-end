import * as React from 'react';
import PropTypes from 'prop-types';
import Button from '@mui/material/Button';
import List from '@mui/material/List';
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';
import { Box, TextField } from '@mui/material';
import { useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';

function CourseTypeAdd(props) {
  const { onClose, open } = props;
  const [typeName, setTypeName] = useState("");
  const [typeImage, setTypeImage] = useState("");

  const createCourseType = () => {
    axios.post(`${process.env.REACT_APP_API_URL}/CourseType/AddCourseType`, {
      type_name: typeName,
      type_image: typeImage
    })
      .then(res => {
        console.log("Success with Response: ", res);
        window.location.reload();
        onClose();
      })
      .catch(error => {
        console.error("Error with Response: ", error.response);
      });
  };
  

  const handleCancel = () => {
    onClose();
  };

  return (
    <Dialog onClose={createCourseType} open={open}>
      <DialogTitle sx={{ width: 375 }} align='center'>Create Course Type</DialogTitle>
      <form>
        <List sx={{ maxWidth: 300, mx: "auto" }}>
          <TextField onChange={(e) => setTypeName(e.target.value)} id="standard-basic" label="Type Name" variant="standard" sx={{ width: 300 }} />
        </List>
        <List sx={{ maxWidth: 300, mx: "auto" }}>
          <TextField onChange={(e) => setTypeImage(e.target.value)} id="standard-basic" label="Type Image" variant="standard" sx={{ width: 300 }} />
        </List>
      </form>
      <Box sx={{ mx: "auto", my: 2 }}>
        <Button onClick={handleCancel} sx={{ backgroundColor: "#EA9E1F", color: "white", width: 155, height: 40, borderRadius: 3, mx: 1, '&:hover': { backgroundColor: "#EA9E1F" } }}>Cancel</Button>
        <Button onClick={createCourseType} sx={{ backgroundColor: "#226957", color: "white", width: 155, height: 40, borderRadius: 3, mx: 1, '&:hover': { backgroundColor: "#226957" } }}>Create</Button>
      </Box>
    </Dialog>
  );
}

CourseTypeAdd.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
};

export default function CourseTypeAddDialog() {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button onClick={handleClickOpen} sx={{ backgroundColor: "#226957", color: "white", width: 135, height: 40, borderRadius: 3, mx: 12, my: 2, '&:hover': { backgroundColor: "#226957" } }}>Add</Button>
      <CourseTypeAdd
        open={open}
        onClose={handleClose}
      />
    </div>
  );
}
