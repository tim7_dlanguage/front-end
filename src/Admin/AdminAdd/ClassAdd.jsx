import * as React from 'react';
import PropTypes from 'prop-types';
import Button from '@mui/material/Button';
import Avatar from '@mui/material/Avatar';
import List from '@mui/material/List';
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';
import { Box, MenuItem, Select, TextField } from '@mui/material';
import { useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';

function Clas(props) {
  const { onClose, open, clas } = props;
  const [courseId, setCourseId] = useState("");
  const [title, setTitle] = useState("");
  const [price, setPrice] = useState("");
  const [imageFile, setImageFile] = useState(null);
  const [description, setDesc] = useState("");

  const create = (e) => {
    const formData = new FormData();
    formData.append("file", imageFile);
  
    axios
      .post(`${process.env.REACT_APP_API_URL}/upload`, formData) // Replace "upload" with your server endpoint for handling file uploads
      .then((response) => {
        const imgPath = response.data.filePath; // Assuming your server responds with the file path
        // Now, you can use imgPath in your API call
        axios
          .post(`${process.env.REACT_APP_API_URL}/Course/AddCourse`, {
            name: title,
            price: price,
            image: imgPath, // Use the uploaded file path here
            typeId: courseId,
          })
          .then((res) => {
            console.log("Success with Response : ", res);
            window.location.reload();
            onClose();
          })
          .catch((error) => {
            console.error("Error With Response : ", error.response);
          });
      })
      .catch((error) => {
        console.error("Error uploading file: ", error);
      });
  };
  

    const [order, setOrder] = useState([]);
    const navigate = useNavigate();
  
    useEffect(() => {
      axios.get(`${process.env.REACT_APP_API_URL}/CourseType/getcoursetypeall`)
          .then(res => {
            setOrder(res.data);
            console.log("Data Accepted");
          })
          .catch(error => {
            console.error("Error fetching data:", error);
          });
    },[]);

  const handleCancel = () => {
    onClose();
  };

  return (
    <Dialog onClose={create} open={open}>
      <DialogTitle sx={{ width: 375 }} align='center'>Create Category</DialogTitle>
      <form>
        <List sx={{ maxWidth: 300 ,mx: "auto"}}>
          <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={courseId}
              label="Category"
              onChange={(e) => setCourseId(e.target.value)}
              sx={{ width: 300 }}
          >
              {order.map((item) => {
                return (
                  <MenuItem value={item.type_id}>{item.type_name}</MenuItem>
                );
              })}
          </Select>
        </List>
        <List sx={{ maxWidth: 300 ,mx: "auto"}}>
          <TextField onChange={(e) => setTitle(e.target.value)} id="standard-basic" label="Title" variant="standard" sx={{ width:300 }}/>
        </List>
        <List sx={{ maxWidth: 300 ,mx: "auto"}}>
          <TextField onChange={(e) => setPrice(e.target.value)} id="standard-basic" label="Price" variant="standard" type='number' sx={{ width:300 }}/>
        </List>
        <List sx={{ maxWidth: 300, mx: "auto" }}>
        <input
          type="file"
          onChange={(e) => setImageFile(e.target.files[0])}
          accept="image/*"
        />
      </List>
      </form>
      <Box sx={{ mx: "auto", my: 2 }}>
        <Button onClick={handleCancel} sx={{backgroundColor: "#EA9E1F", color: "white", width: 155, height: 40, borderRadius: 3, mx: 1, '&:hover': {backgroundColor: "#EA9E1F"}}}>Cancel</Button>
        <Button onClick={create} sx={{backgroundColor: "#226957", color: "white", width: 155, height: 40, borderRadius: 3, mx: 1, '&:hover': {backgroundColor: "#226957"}}}>Create</Button>
      </Box>
    </Dialog>
  );
}

Clas.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
};

export default function ClassAdd() {
  const [open, setOpen] = useState(false);
  const [clas, setCls] = useState([]);

  useEffect(() => {
    axios.get(`${process.env.REACT_APP_API_URL}/AdminCourse`)
    .then(res => {
      console.log("Success with Response : ", res);
      setCls(res.data)
    })
    .catch(error => {
      console.error("Error With Response : ", error.response);
    })
  }, []);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button onClick={handleClickOpen} sx={{backgroundColor: "#226957", color: "white", width: 135, height: 40, borderRadius: 3, mx: 12, my:2, '&:hover': {backgroundColor: "#226957"}}}>Add</Button>
      <Clas
        open={open}
        onClose={handleClose}
        clas={clas}
      />
    </div>
  );
}
