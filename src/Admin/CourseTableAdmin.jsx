import React, { useContext, useEffect, useState } from 'react'
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { Button } from '@mui/material';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import ClassAdd from './AdminAdd/ClassAdd';
import ClassEdit from './AdminEdit/ClassEdit';
import { AuthContext } from '../contexts/AuthContext'

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: "#226957",
    color: theme.palette.common.white,
    fontWeight: 700,
    fontSize: 16,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 16,
    height: 55,
    width: 100
  },
}));

const StyledTableRow = styled(TableRow)(() => ({
  '&:nth-of-type(even)': {
    backgroundColor: '#EA9E1F33',
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));

export default function ClassTableAdmin() {
  const { apiUrl } = useContext(AuthContext)
  const [order, setOrder] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    axios.get(`${apiUrl}/Course/getcourseall`)
        .then(res => {
          setOrder(res.data);
          console.log("Data Accepted");
        })
        .catch(error => {
          console.error("Error fetching data:", error);
        });
  },[]);


  const handleDelete = (id) => {
    axios.delete(`${process.env.REACT_APP_API_URL}/Course/DeleteCourseById?id=${id}`)
      .then(res => {
        console.log("Success with Response: ", res);
        // Refresh the table after successful deletion
        window.location.reload(true);
      })
      .catch(error => {
        console.error("Error with Response: ", error.response);
      });
  };

  const active = (id) => {
    axios.patch(`${process.env.REACT_APP_API_URL}/Course/ToggleCourseStatus?id=${id}`)
    .then(res => {
      console.log("Succes With Response : ", res);
      window.alert("Course berhasil diubah");
      window.location.reload(true);
    })
    .catch(error => {
      console.error("Error with response : ", error.response.data)
    });
  }
  
  return (
    <div>
      <ClassAdd />
      <TableContainer component={Paper} sx={{ boxShadow: "none" }}>
        <Table sx={{ maxWidth: 1400, mx: "auto" }} aria-label="customized table">
          <TableHead>
            <TableRow>
              <StyledTableCell align='center'>No</StyledTableCell>
              <StyledTableCell align='center'>Course Id</StyledTableCell>
              <StyledTableCell align='center'>price</StyledTableCell>
              <StyledTableCell align='center'>Image</StyledTableCell>
              <StyledTableCell align='center'>Language</StyledTableCell>
              <StyledTableCell align='center'>Active/Inactive</StyledTableCell>
              <StyledTableCell align='center'>CourseName</StyledTableCell>
              <StyledTableCell align='center'>Action</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {order.map((row, index) => (
              <StyledTableRow key={row.classId}>
                <StyledTableCell component="th" scope="row" align='center'>
                  {index+1}
                </StyledTableCell>
                <StyledTableCell align='center'>{row.id}</StyledTableCell>
                <StyledTableCell align='center'>{row.price}</StyledTableCell>
                <StyledTableCell align='center'><img style={{height: '150px'}} src={row.image} alt=""/></StyledTableCell>
                <StyledTableCell align='center'>{row.type}</StyledTableCell>
                <StyledTableCell align='center'>
                  {row.active === 1 ? 'Active' : 'Inactive'}
                </StyledTableCell>
                <StyledTableCell align='center'>{row.name}</StyledTableCell>
                <StyledTableCell align='center'>
                  <ClassEdit order={row}/>
                  <Button onClick={() => active(row.id)} sx={{backgroundColor: "#b35a12", color: "white", width: 150, height: 30, mt: 2, borderRadius: 2, '&:hover': {backgroundColor: "#d6ed2b"}}}>Toggle Status</Button>
                </StyledTableCell>
              </StyledTableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
}
